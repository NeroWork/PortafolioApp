# Author
**Furlan Facundo:**

LinkedIn: https://www.linkedin.com/in/facundo-furlan/

GitHub: https://github.com/NeroWork

WebPage: [Portfolio](https://nerowork.github.io/PortafolioApp/)



# Welcome to my Portfolio!

This is a little WebPage I created so anyone could get to know me and my job.


# Technologies
For the project I mainly used ReactJS and Bootstrap.


# Dependencies
react: ^18.2.0

bootstrap: ^5.2.3

react-bootstrap: ^2.7.0
